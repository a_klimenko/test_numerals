<?php

function GetIntWord(int $countItems, string $firstPres, string $secondPres, string $thirdPres) 
{
    $rest = $countItems % 10;
    if ($rest == 0 || $rest > 4) {
        return $countItems . " " . $thirdPresentation;
    } elseif ($rest == 1) {
        return $countItems . " " . $firstPresentation;
    } else {
        return $countItems . " " . $secondPresentation;
    }
}

echo GetIntWord(2, 'день', 'дня', 'дней');